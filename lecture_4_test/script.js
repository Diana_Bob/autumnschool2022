window.addEventListener('DOMContentLoaded', () => {
  const box = document.querySelector('#box')

  const button = document.querySelector('button')

  const massive = ['cat', 'dog', 'frog']

  // console.log(button)
  const circles = document.querySelectorAll('.circle')

  // console.log(circles)
  const wrapper = document.querySelector('.wrapper')
  //ищем селекторы внутри элемента wrapper
  const oneCircle = wrapper.querySelector('.circle')

  // inline стили приорететнее внешенеподключенных
  // box.style.backgroundColor = 'blue'
  // box.style.width = '500px'
  button.style.borderRadius = '100%'
  // circles[0].style.backgroundColor = 'red'
  // прописываем стили в виде текста
  // пишем, как в обычном css
  box.style.cssText = 'background-color: blue; width: 500px'
  // для назначения стилей в впсевдомассиве можем использовать циклы
  for (let i = 0; i < circles.length; i++) {
    circles[i].style.backgroundColor = 'green'
  }
  // или единственный метод forEach
  // circles.forEach((item) => {
  //   item.style.backgroundColor = 'black'
  // })

  // создаем элемент в js (сейчас он существует только в js)
  const div = document.createElement('div')

  div.innerHTML = '<h2>H2 header</h2>'
  console.log(div)
  // работаем с css классами
  div.classList.add('red')
  // if (div.classList.contains('red')) {
  //   console.log('have red class')
  // }
  // div.classList.remove('red')
  // div.classList.toggle('red')
  // в элемент с класссом '.wrapper' вставляем в конце div
  // document.querySelector('.wrapper').append(div)
  // тоже самое
  // wrapper.append(div)
  //  вставка в начала элемента
  // wrapper.prepend(div)
  // вставка div до hearts[0]
  // oneCircle.before(div)
  // вставка div после hearts[0]
  oneCircle.after(div)
  // современное удаление элемента
  oneCircle.remove()
  // впишем текст в div
  // также можем вставлять HTML структуру
  // предназначен для изменения HTML разработчиками
  // div.innerHTML = '<h2>Hello, world!<h2>'
  // второй способ вставки текста
  // сюда HTML писать нельзя!
  // используется для получения строк от пользовате
  // div.textContent = '<h2>H2 header</h2>'
  button.addEventListener('click', (event) => {
    event.target.classList.toggle('black')
  })
})
